import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addHandler, fetchList} from "../../store/actions";
import './ToDo.css'

const ToDo = () => {

    const dispatch = useDispatch();
    const list = useSelector(state => state.list);

    const [addText, setAddText] = useState('');


    useEffect(() => {
        dispatch(fetchList());
    }, [dispatch]);

    const onChangeHandler = async (e) => {
        const value = e.target.value
        setAddText(value)
    }

    const createNewPost = (e) => {
        e.preventDefault();
        const obj = {
            text: addText
        }
        dispatch(addHandler(obj))
    }

    return (
        <div className="list">
            <form onSubmit={createNewPost}>
                <input type="text" onChange={onChangeHandler}/>
                <button type={'submit'}>Add</button>
            </form>
            <div className="main">
                {list && Object.keys(list).map(key => (
                    <div key={key}>
                        <p>{list[key].text}</p>
                    </div>

                    ))}
                {console.log(list)}
            </div>
        </div>
    );
};

export default ToDo;