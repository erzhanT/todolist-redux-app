import axios from "axios";
const url = 'https://todo-erzhan-default-rtdb.firebaseio.com/list.json'

export const FETCH_LIST_REQUEST = 'FETCH_LIST_REQUEST';
export const FETCH_LIST_SUCCESS = 'FETCH_LIST_SUCCESS';
export const FETCH_LIST_FAILURE = 'FETCH_LIST_FAILURE';


export const fetchListRequest = () => ({type: FETCH_LIST_REQUEST});
export const fetchListSuccess = list => ({type: FETCH_LIST_SUCCESS, list});
export const fetchListFailure = () => ({type: FETCH_LIST_FAILURE});

export const fetchList = () => {
    return async dispatch => {
        dispatch(fetchListRequest());
        try {
            const response = await axios.get(url);
            dispatch(fetchListSuccess(response.data))
            console.log(response.data);
        } catch (e) {
            dispatch(fetchListFailure());
        }
    };
};

export const addHandler = (data) => {
    return async dispatch => {
        dispatch(fetchListRequest());
        try {
            await axios.post(url, data);
            dispatch(fetchList())
        } catch (e) {
            console.error(e);
        }
    }
}

