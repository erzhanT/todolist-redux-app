import {FETCH_LIST_FAILURE, FETCH_LIST_REQUEST, FETCH_LIST_SUCCESS} from "./actions";

const initialState = {
    list: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_LIST_REQUEST: {
            return {...state}
        }
        case FETCH_LIST_SUCCESS: {
            return {...state,  list: action.list}
        }
        case FETCH_LIST_FAILURE: {
            return {...state}
        }
    }
    return state
}


export default reducer;